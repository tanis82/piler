Disclosure policy

If you find a security issue, please contact the project owner at sj@acts.hu
with the details (ie. piler version, details of the setup, how to exploit the
vulnerability, etc).

Please provide 30 days for verifying the vulnerability, fixing the issue, and
notifying the piler users.

Security update policy

If a security vulnerability has found, the details, possible mitigations,
workarounds, etc. will be shared on the piler mailing list (piler-user@list.acts.hu)
and on the wiki: https://www.mailpiler.org/

Security configurations

 - Use https for the GUI
 - Reset the default passwords for admin and auditor
 - Use the smtp acl feature to restrict SMTP access to the archive, see https://mailpiler.com/smtp-acl-list/
